
<?php /* POST FORMAT - DEFAULT */ ?>
<?php $defaultargs = array('class' => 'img-responsive'); ?>
<article id="post-<?php the_ID(); ?>" class="the-single col-md-9 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
    <header>
        <h1 itemprop="name"><?php the_title(); ?></h1>
        <div class="date-container">
            <span class="date">Fecha: <?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
            <span class="author">Publicado por: <?php the_author_posts_link(); ?></span>
        </div>
    </header>
    <div class="post-content" itemprop="articleBody">
        <?php the_content() ?>
        <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'vzbroward' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
        <footer>
            <div class="single-categories"><?php the_category(' '); // Separated by commas ?></div>
            <div class="single-categories"><?php the_tags( __( 'Tags: ', 'vzbroward' ), ' ', '<br>');  ?></div>
        </footer>
    </div><!-- .post-content -->
    <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
    <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
    <meta itemprop="url" content="<?php the_permalink() ?>">
    <?php if ( comments_open() ) { comments_template(); } ?>
</article> <?php // end article ?>
