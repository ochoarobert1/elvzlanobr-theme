<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-md-4">
                        <?php dynamic_sidebar( 'footer_box1' ); ?>
                    </div>
                    <div class="footer-item col-md-4">
                        <?php dynamic_sidebar( 'footer_box2' ); ?>
                    </div>
                    <div class="footer-item col-md-4">
                        <?php dynamic_sidebar( 'footer_box3' ); ?>
                    </div>
                </div>
            </div>
            <div class="footer-copy col-md-12 no-paddingl no-paddingr">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="" class="img-responsive">
                            </div>
                            <div class="col-md-10">
                                <h4>El Venezolano de Broward</h4>
                                <h4>2016 &copy; - Todos los derechos reservados</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Desarrollado por Robert Ochoa</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
