<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_functions_overrides.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function vzbroward_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltheme-css', get_template_directory_uri() . '/css/owl.theme.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltrans-css', get_template_directory_uri() . '/css/owl.transitions.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME -*/
            wp_register_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL -*/
            wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL -*/
            wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL -*/
            wp_register_style('owltrans-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');
        }

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/vzbroward-style.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/vzbroward-mediaqueries.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');

        /*- WORDPRESS STYLE -*/
        wp_register_style('wp-initial-style', get_template_directory_uri() . '/style.css', false, $version_remove, 'all');
        wp_enqueue_style('wp-initial-style');
    }
}

add_action('init', 'vzbroward_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', true);
    }
    wp_enqueue_script('jquery');
}


function vzbroward_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '2.0.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.pkgd.min.js', array('jquery'), '1.2.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.0/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'vzbroward_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'vzbroward', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
    'default-image' => '',    // background image default
    'default-color' => '',    // background color default (dont add the #)
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
)
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function vzbroward_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'vzbroward_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'vzbroward' ),
    'nav_menu' => __( 'Menu Navegador Principal', 'vzbroward' ),
    'footer_menu' => __( 'Menu Footer', 'vzbroward' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'vzbroward_widgets_init' );
function vzbroward_widgets_init() {

    register_sidebars( 12, array(
        'name'          => __('Home - Posicion %d'),
        'id'            => 'home_sidebar',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Publicidad Grande Internas', 'vzbroward' ),
        'id' => 'big_main_sidebar',
        'description' => __( 'Publicidad En área superior en archivos y entradas', 'vzbroward' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Publicidad - Sidebar en internas', 'vzbroward' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'vzbroward' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 3, array(
        'name'          => __('Footer - Posicion %d'),
        'id'            => 'footer_box',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #1F191B !important;
            background-image:url(' . get_template_directory_uri() . '/images/bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important; }
        .login form{
            -webkit-border-radius: 5px;
            border-radius: 5px;
            background-color: rgba(203, 203, 203, 0.5);
            box-shadow: 0px 0px 6px #878787;
        }
        .login label{
            color: black;
            font-weight: 500;
        }
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'vzbroward' );
        echo '<a href="http://wordpress.org/" >WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'vzbroward' );
        echo '<a href="http://robertochoa.com.ve/" > Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */


add_filter( 'rwmb_meta_boxes', 'vzbroward_metabox' );

function vzbroward_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'home',
        'title'      => __( 'Organizador del Inicio', 'vzbroward' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
        // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
        'relation'        => 'OR',
        // List of post slugs. Can be array or comma separated. Optional.
        'slug'            => array( 'inicio' ),
        // List of page templates. Can be array or comma separated. Optional.
        'template'        => array( 'page-inicio.php' )
    ),
        'fields' => array(
        array(
        'name'  => __( 'Full name', 'textdomain' ),
        'desc'  => 'Format: First Last',
        'id'    => $prefix . 'fname',
        'type'  => 'text',
        'std'   => 'Anh Tran',
        'class' => 'custom-class',
    ),
        array(
        'name' => 'Group', // Optional
        'id' => 'group_id',
        'type' => 'group',
        // List of sub-fields
        'fields' => array(
        array(
        'name' => 'Text',
        'id' => 'text',
        'type' => 'text',
    ),
        // Other sub-fields here
    ),
    ),
    )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */
// Register Custom Post Type
function clasificados() {

    $labels = array(
        'name'                  => _x( 'Clasificados', 'Post Type General Name', 'vzbroward' ),
        'singular_name'         => _x( 'Clasificado', 'Post Type Singular Name', 'vzbroward' ),
        'menu_name'             => __( 'Clasificados', 'vzbroward' ),
        'name_admin_bar'        => __( 'Clasificados', 'vzbroward' ),
        'archives'              => __( 'Archivo de Clasificados', 'vzbroward' ),
        'parent_item_colon'     => __( 'Clasificado Padre:', 'vzbroward' ),
        'all_items'             => __( 'Todos los Clasificados', 'vzbroward' ),
        'add_new_item'          => __( 'Agregar Nuevo Clasificado', 'vzbroward' ),
        'add_new'               => __( 'Agregar Nuevo', 'vzbroward' ),
        'new_item'              => __( 'Nuevo Clasificado', 'vzbroward' ),
        'edit_item'             => __( 'Editar Clasificado', 'vzbroward' ),
        'update_item'           => __( 'Actualizar Clasificado', 'vzbroward' ),
        'view_item'             => __( 'Ver Clasificado', 'vzbroward' ),
        'search_items'          => __( 'Buscar Clasificados', 'vzbroward' ),
        'not_found'             => __( 'No hay resultados', 'vzbroward' ),
        'not_found_in_trash'    => __( 'No hay resultados en papelera', 'vzbroward' ),
        'featured_image'        => __( 'Imagen Destacada', 'vzbroward' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'vzbroward' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'vzbroward' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'vzbroward' ),
        'insert_into_item'      => __( 'Insetar dentro de Clasificados', 'vzbroward' ),
        'uploaded_to_this_item' => __( 'Cargado a este clasificado', 'vzbroward' ),
        'items_list'            => __( 'Listado de Clasificados', 'vzbroward' ),
        'items_list_navigation' => __( 'Navegador del Listado de Clasificados', 'vzbroward' ),
        'filter_items_list'     => __( 'Filtro del Listado de Clasificados', 'vzbroward' ),
    );
    $args = array(
        'label'                 => __( 'Clasificado', 'vzbroward' ),
        'description'           => __( 'Clasificados dentro del Sitio', 'vzbroward' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'custom_clasificados' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-id-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'clasificados', $args );

}
add_action( 'init', 'clasificados', 0 );

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('featured_tall', 570, 467, array( 'center', 'center' ));
    add_image_size('featured_wide', 570, 290, true);
    add_image_size('featured_small', 285, 173, true);
    add_image_size('block_section', 360, 240, array( 'center', 'center' ));
    add_image_size('block_special', 250, 250, true);

}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

//require_once('includes/wp_woocommerce_functions.php');

add_action('wp_head', 'custom_url_hook', 10);

function custom_url_hook() {
    echo '<script type="text/javascript"> var ruta_blog = "' . esc_url(get_template_directory_uri()) .'"</script>';
}

// Creating the widget 
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'wpb_widget', 

            // Widget name will appear in UI
            __('Mostrar Noticias por Categorias', 'vzbroward'), 

            // Widget description
            array( 'description' => __( 'Widget Personalizado para Broward', 'vzbroward' ), ) 
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $categoria = $instance['categoria'];
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title']; ?>
<?php $args2 = array('post_type' => 'post', 'posts_per_page' => 7, 'cat' => $categoria, 'order' => 'DESC', 'orderby' => 'date'); ?>
<?php query_posts($args2); ?>
<?php while (have_posts()) : the_post(); ?>
<div class="custom-widget-news-item col-md-12 no-paddingl no-paddingr">
    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
        <span class="block-date"><i class="fa fa-clock-o"></i> <?php echo get_the_date("F d, Y", get_the_ID()); ?></span>
        <h3><?php the_title(); ?></h3>
    </a>
</div>
<?php endwhile; wp_reset_query(); wp_reset_postdata(); ?>
<?php echo $args['after_widget'];
    }

    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        if ( isset( $instance[ 'categoria' ] ) ) {
            $categoria = $instance[ 'categoria' ];
        }
        else {
            $categoria = __( 'categoria', 'wpb_widget_domain' );
        }
        // Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titulo:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    <label for="<?php echo $this->get_field_id( 'categoria' ); ?>"><?php _e( 'Categoria:' ); ?></label> 
    <select class="widefat" name="<?php echo $this->get_field_name( 'categoria' ); ?>" id="<?php echo $this->get_field_id( 'categoria' ); ?>">
        <?php $categories = get_categories( array( 'orderby' => 'name', 'order'   => 'ASC' ) ); ?>
        <?php foreach ($categories as $item) { ?>
        <option value="<?php echo $item->term_id; ?>" <?php if ($categoria == $item->term_id) { echo 'selected'; } ?>><?php echo $item->name; ?></option>
        <?php } ?>
    </select>

</p>
<?php 
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['categoria'] = ( ! empty( $new_instance['categoria'] ) ) ? strip_tags( $new_instance['categoria'] ) : '';

        return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );


?>
