<!--  SECCIÓN OPINION -->
<section class="block-section col-md-12 no-paddingl no-paddingr">
    <div class="block-section-title col-md-12">
        <div class="block-section-title-inner-html col-md-12">
            <span>OPINIÓN</span>
            <span>Ver Categoría >></span>
        </div>
    </div>
    <div class="block-opinion-container col-md-8 no-paddingl no-paddingr">
        <?php $args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'category_name' => $categoria, 'ignore_sticky_posts' => 1, 'post__not_in' => $posted_ids); ?>
        <?php query_posts($args); ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php array_push($posted_ids, get_the_ID()); ?>
        <article class="block-item block-opinion-item col-md-12">
            <div class="block-item-inner-html col-md-12 no-paddingl no-paddingr">
                <div class="col-md-2 no-paddingl no-paddingr">
                    <?php if (has_post_thumbnail()) { ?>
                    <?php the_post_thumbnail('block_section_small', $defaultatts); ?>
                    <?php } else { ?>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                    <?php } ?>
                </div>
                <div class="col-md-10 no-paddingr">
                    <h4>Richard Silva Romero</h4>
                    <h3><?php the_title(); ?></h3>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </article>
        <?php endwhile; wp_reset_query(); ?>
    </div>
    <div class="col-md-4">
        <?php $args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'category_name' => $categoria, 'ignore_sticky_posts' => 1, 'post__not_in' => $posted_ids); ?>
        <?php query_posts($args); ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php array_push($posted_ids, get_the_ID()); ?>
        <article class="block-item block-small-item col-md-12">
            <div class="col-md-3 no-paddingl no-paddingr">
                 <?php if (has_post_thumbnail()) { ?>
                    <?php the_post_thumbnail('block_section_small', $defaultatts); ?>
                    <?php } else { ?>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                    <?php } ?>
            </div>
            <div class="col-md-9 no-paddingr">
                <h3><?php the_title(); ?></h3>
                <span class="block-date"><?php echo get_the_date('d-m-Y'); ?></span>
            </div>
        </article>
        <?php endwhile; wp_reset_query(); ?>
    </div>
</section>
<!--  SECCIÓN DESTACADOS -->
<!--
<section class="block-section col-md-12 no-paddingl no-paddingr">
    <article class="col-md-4 no-paddingl">
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Editorial Recomendado</span>
            </div>
        </div>
        <div class="block-section-content col-md-12">
            <div class="block-section-content-inner-html col-md-12">
                <img src="<?php /*  echo esc_url(get_template_directory_uri()); ?>/images/test/nacional.jpg" alt="" class="img-responsive"/>
                <h4>Caracas, Venezuela</h4>
                <h3>La frontera de un cínico</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A voluptate, ad officia architecto illo! Delectus eos doloribus, nulla impedit similique ex? Itaque sequi, ipsum nam.</p>
            </div>
        </div>
    </article>
    <article class="col-md-4 no-paddingr">
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Dános tu Opinión</span>
            </div>
        </div>
    </article>
    <article class="col-md-4 no-paddingr">
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Caricatura del Día</span>
            </div>
        </div>
        <div class="block-section-content col-md-12">
            <img src="<?php echo esc_url(get_template_directory_uri()); */ ?>/images/test/Captura.jpg" alt="" class="img-responsive"/>
        </div>
    </article>
</section>
-->
