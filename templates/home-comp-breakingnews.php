<?php $args = array('posts_per_page' => 3, 'ignore_sticky_posts' => 1); ?>
<?php query_posts($args); ?>
<?php if (have_posts()) :?>
<section class="breaking-news-container col-md-12 no-paddingl no-paddingr">
    <div class="col-md-2"><span class="breaking-label">AHORA MISMO:</span></div>
    <div class="breaking-news-content col-md-9 no-paddingl">
        <div class="breaking-news-owl col-md-12 no-paddingl no-paddingr">
            <?php while (have_posts()) : the_post(); ?>
            <article class="breaking-item" itemscope itemtype="http://schema.org/Article">
                <a href="<?php the_permalink(); ?>">
                    <h3 itemprop="headline"><?php the_title(); ?></h3>
                </a>
            </article>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="col-md-1 no-paddingl no-paddingr"><i class="fa fa-chevron-left breaking-news-left"></i><i class="fa fa-chevron-right breaking-news-right"></i></div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>
