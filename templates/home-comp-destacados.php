<?php $defaultatts = array('class' => 'img-responsive'); ?>
<section class="featured-news-container col-md-12 no-paddingl no-paddingr">
    <?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'category_name' => 'home-destacados', 'ignore_sticky_posts' => 1); ?>
    <?php query_posts($args); $i = 1; ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php array_push($posted_ids, get_the_ID());  ?>
    <?php if ($i == 1) { ?>
    <div class="col-md-6 no-paddingr">
        <article class="featured-item featured-item-tall col-md-12 no-paddingl no-paddingr">
            <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                <div class="featured-item-mask">
                    <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                        <h2><?php the_title(); ?></h2>
                    </a>
                </div>
            </a>
            <?php the_post_thumbnail('featured_tall', $defaultatts); ?>
        </article>
    </div>
    <?php } ?>
    <?php if ($i == 2) { ?>
    <div class="col-md-6 no-paddingl">
        <article class="featured-item featured-item-wide col-md-12 no-paddingl no-paddingr">
            <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                <div class="featured-item-mask">
                    <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                        <h2><?php the_title(); ?></h2>
                    </a>
                </div>
                <?php the_post_thumbnail('featured_wide', $defaultatts); ?>
            </a>
        </article>

        <?php } ?>
        <?php if ($i >= 3) { ?>
        <div class="col-md-6 no-paddingl no-paddingr">
            <article class="featured-item featured-item-small featured-item-small-<?php echo $i; ?> col-md-12 no-paddingl no-paddingr">
                <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                    <div class="featured-item-mask">
                      
                        <a href="<?php the_permalink()?>" title="<?php echo get_the_title(); ?>">
                            <h2><?php the_title(); ?></h2>
                        </a>
                    </div>
                </a>
                <?php the_post_thumbnail('featured_small', $defaultatts); ?>
            </article>
        </div>
        <?php } ?>
        <?php if ($i == 4) { ?>
    </div>
    <?php } ?>
    <?php $i++; endwhile; wp_reset_query(); ?>
</section>
