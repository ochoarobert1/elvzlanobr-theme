<?php if ($categoria != '') { ?>
<?php $term = get_term_by('name', $categoria, 'category'); ?>
<?php } ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<section class="block-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="block-section-main-container col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingl">
        <?php if ($categoria == '') { ?>
        <?php $args = array('post_type' => 'post', 'posts_per_page' => $num_not, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $posted_ids); ?>
        <?php } else {  ?>
        <?php $args = array('post_type' => 'post', 'posts_per_page' => $num_not, 'order' => 'DESC', 'orderby' => 'date', 'cat' => $term->term_id, 'ignore_sticky_posts' => 1, 'post__not_in' => $posted_ids); ?>
        <?php  }  ?>
        <?php query_posts($args); ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php array_push($posted_ids, get_the_ID()); ?>

        <article class="block-item col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <picture class="block-item-img col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <span class="block-category">
                    <?php echo $term->term_name; ?>
                </span>
                <?php if (has_post_thumbnail()) { ?>
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('block_section', $defaultatts); ?>
                </a>
                <div class="block-item-img-mask">
                    <span class="block-date"><i class="fa fa-clock-o"></i> <?php echo get_the_date("F d, Y", get_the_ID()); ?></span>
                    <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title();?></h3>
                    </a>
                </div>
                <?php } else { ?>
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                <div class="block-item-img-mask">
                    <span class="block-date"><i class="fa fa-clock-o"></i> <?php echo get_the_date("F d, Y", get_the_ID()); ?></span>
                    <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title();?></h3>
                    </a>
                </div>
                <?php } ?>
            </picture>
        </article>
        <?php endwhile; wp_reset_query(); wp_reset_postdata(); ?>
    </div>
    <div class="block-section-ads-container col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <?php dynamic_sidebar( 'home_sidebar-' . $sidebar ); ?>
    </div>
</section>
<?php $categoria = ''; ?>
