<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#1F191B" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#1F191B" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="El Venezolano Networks" />
        <meta name="copyright" content="http://elvenezolanodebroward.com" />
        <meta name="geo.position" content="25.9805677,-80.3394236" />
        <meta name="ICBM" content="25.9805677,-80.3394236" />
        <meta name="geo.region" content="US" />
        <meta name="geo.placename" content="14359 Miramar Parkway, Suite 272  Miramar, Fl 33027" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="pre-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="header-info col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <?php setlocale(LC_ALL,"es_ES@euro","es_ES","esp"); $now = new DateTime(null, new DateTimeZone('America/Caracas'));  ?>
                                <span><?php echo $now->format("F d, Y");  ?></span>
                            </div>
                            <div class="header-menu col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div', 'menu_class' => 'header-menu-container' )); ?>
                            </div>
                            <div class="header-social col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-instagram"></i>
                                <i class="fa fa-youtube"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-paddingl">
                                <a href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo('name')?>">
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name')?>" class="img-logo" title="<?php echo get_bloginfo('name')?>"/>
                                </a>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"></div>
                            <div class="clearfix"></div>
                            <nav class="the-navbar col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php wp_nav_menu( array( 'theme_location' => 'nav_menu', 'depth' => 2, 'container' => 'div', 'menu_class' => 'header-nav-menu' )); ?>
                                <ul class="header-nav-menu header-nav-menu-fixed">
                                    <li>
                                        <a href="http://elvenezolano.tv/"><span class="red-dot"></span> Señal En Vivo</a>
                                    </li>
                                    <li>
                                        <a><span class="glyphicon glyphicon-search"></span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
