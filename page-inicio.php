<?php get_header(); ?>
<?php the_post(); ?>
<?php $posted_ids = array(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <!-- ADS -->
        <section class="ads-container ads-container-single col-md-12 no-paddingl no-paddingr">
            <div class="col-md-12">
                <?php dynamic_sidebar( 'home_sidebar' ); ?>
            </div>
        </section>
        <!-- FEATURED ITEMS / DESTACADO DE NOTICIAS -->
        <?php include( locate_template( 'templates/home-comp-destacados.php') ); ?>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Venezuela</span>
            </div>
        </div>
        <!--  SECCIÓN VENEZUELA -->
        <?php $num_not = 4; $categoria = 'venezuela'; $sidebar = 2; ?>
        <?php include( locate_template( 'templates/home-comp-blocks.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 3; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
        <!-- ADS -->
        <?php $sidebar = 4; ?>
        <div class="clearfix"></div>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>EEUU</span>
            </div>
        </div>
        <!--  SECCIÓN 2 -->
        <?php $num_not = 4; $categoria = 'eeuu'; $sidebar = 5; ?>
        <?php include( locate_template( 'templates/home-comp-blocks.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 6; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
        <!--  SECCIÓN 2 -->
        <div class="clearfix"></div>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Broward</span>
            </div>
        </div>
        <?php $num_not = 4; $categoria = 'broward'; $sidebar = 7; ?>
        <?php include( locate_template( 'templates/home-comp-blocks.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 8; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
        <!--  SECCIÓN 2 -->
        <div class="clearfix"></div>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Internacionales</span>
            </div>
        </div>
        <?php $num_not = 4; $categoria = 'internacionales'; $sidebar = 9; ?>
        <?php include( locate_template( 'templates/home-comp-blocks.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 10; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
        <!--  SECCIÓN 2 -->
        <div class="clearfix"></div>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Deportes</span>
            </div>
        </div>
        <?php $num_not = 4; $categoria = 'deportes'; ?>
        <?php include( locate_template( 'templates/home-comp-special.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 11; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
        <!--  SECCIÓN 2 -->
        <div class="clearfix"></div>
        <div class="block-section-title col-md-12">
            <div class="block-section-title-inner-html col-md-12">
                <span>Entretenimiento</span>
            </div>
        </div>
        <?php $num_not = 4; $categoria = 'entretenimiento'; ?>
        <?php include( locate_template( 'templates/home-comp-special.php' ) ); ?>
        <!-- ADS -->
        <?php $sidebar = 12; ?>
        <?php include( locate_template( 'templates/gen-comp-ads-wide.php') ); ?>
    </div>
</main>
<?php get_footer(); ?>




