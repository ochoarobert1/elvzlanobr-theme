<?php get_header(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="ads-container ads-container-single col-md-12 no-paddingl no-paddingr">
            <div class="col-md-12">
                <?php dynamic_sidebar( 'big_main_sidebar' ); ?>
            </div>
        </section>
        <section class="col-md-12">
            <h1><?php single_cat_title(); ?></h1>
            <hr>
            <div class="col-md-9">
                <?php $defaultatts = array('class' => 'img-responsive'); ?>
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <article class="block-item col-md-6">
                    <picture class="block-item-img col-md-12 no-paddingl no-paddingr">
                        <?php if (has_post_thumbnail()) { ?>
                        <?php the_post_thumbnail('block_section', $defaultatts); ?>
                        <div class="block-item-img-mask">
                            <span class="block-date"><?php echo get_the_date("F d, Y", get_the_ID()); ?></span>
                            <a href="<?php the_permalink(); ?>">
                                <h3><?php the_title();?></h3>
                            </a>
                        </div>
                        <?php } else { ?>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                        <div class="block-item-img-mask">
                            <span class="block-date"><?php echo get_the_date("F d, Y", get_the_ID()); ?></span>
                            <a href="<?php the_permalink(); ?>">
                                <h3><?php the_title();?></h3>
                            </a>
                        </div>
                        <?php } ?>
                    </picture>
                </article>
                <?php endwhile; ?>
                <div class="pagination col-md-12">
                    <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                </div>
            </div>
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
            <?php else: ?>
            <article>
                <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
            </article>
            <?php endif; ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
